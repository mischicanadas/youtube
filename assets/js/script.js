$(document).ready(function(){

    $('#txt_url_link').bind("change keyup input", function() {
                    
        if(!Validate_URL(true))
        {
            $("#img_preview").attr("src",'');
            $("#txt_custom_name").val("");
        }
            
    });

    $("#btn_preview").on("click",function(){

        Validate_URL(true);
    });

    $("#btn_process").on("click",function(){
        if(Validate_URL(true))
        {
            var _url = $("#txt_url_link").val();
            var _filename = $("#txt_custom_name").val();
            //var _watermark = $("#txt_watermark").val();
            var _format = $('input:radio[name=selector]:checked').val();

            if(_format == null)
            {
                $('#Modal_Message_Body_Title').html('Please select media format option Audio or Video');
                $('#Modal_Message').modal('show');
            }
            else
            {
                if(_filename === '')
                    _filename = "youtube_"+get_date();
                else
                    _filename += "_"+get_date();
                /*
                if(_watermark === '')
                    _watermark = "WaterMark: No Piratees!";
                */
           
                Process_Media(_url,_format,_filename);
            }
            
        }
    });

    function Validate_URL(displayMessage)
    {
        var isValid = false;
        var _url = $("#txt_url_link").val();

        if(_url === '')
        {
            isValid = false;
            if(displayMessage)
            {
                $('#Modal_Message_Body_Title').html('URL Link is empty');
                $('#Modal_Message').modal('show');
            }            
        }
        else
        {
            var isValid = Youtube.isValid(_url);
            
            if(isValid)
            {
                var thumb = Youtube.thumb(_url,'big');
                $("#img_preview").attr("src",thumb);

                var _info = Youtube.info(_url);
                _info.done(function(data){
                    
                    if(data.error == null)
                    {
                        $("#txt_custom_name").val(data.title.replace(/[^a-zA-Z0-9]/g,'_').replace(/_{2,}/g,'_'));
                        isValid = true;
                    }
                    else
                    {
                        isValid = false;
                        $("#txt_custom_name").val("");
                        if(displayMessage)
                        {
                            $('#Modal_Message_Body_Title').html('Youtube error: '+data.error);
                            $('#Modal_Message').modal('show');
                        }
                    }
                        
                });                
            }
            else
            {
                isValid = false;
                if(displayMessage)
                {
                    $('#Modal_Message_Body_Title').html('Youtube link not valid');
                    $('#Modal_Message').modal('show');
                }
            }            
            
        }
        return isValid;
    }

    function Process_Media(_url,_format,_filename)
    {
        $.LoadingOverlaySetup({
            color           : "rgba(0, 0, 0, 0.8)",
            image           : "assets/img/ring.svg",
            maxSize         : "80px",
            minSize         : "20px",
            resizeInterval  : 0,
            size            : "50%"
        });
        // Show full page LoadingOverlay
        $.LoadingOverlay("show");

        $.ajax({
                type: "POST",
                url: "assets/php/process.php",
                data: {Action:'Youtube',URL:_url,Format:_format,Filename:_filename},
                dataType: "json",
                success: function(data)
                {
                    //Close the LoadingOverlay
                    $.LoadingOverlay("hide");

                    //Append to table
                    var _row = "";
                    _row += "<tr>";
                    _row +=     "<td>"+_url+"</td>";
                    _row +=     "<td>"+_format+"</td>";
                    _row +=     "<td><a href='assets/php/"+data.filename+"' target='_blank'>"+_filename+"</a></td>";
                    _row += "</tr>"+$("#tbody_links").html();

                    $("#tbody_links").html(_row);

                    $('#Modal_Message_Body_Title').html('<a href="assets/php/'+data.filename+'" target="_blank">Download here</a>');
                    $('#Modal_Message').modal('show');
                }        
            });
    }

    function get_date()
    {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        var hh = today.getHours();
        var min = today.getMinutes();
        var ss = today.getSeconds();

        if(dd < 10)
            dd = "0"+dd;
        if(mm < 10)
            mm = "0"+mm;
        if(hh < 10)
            hh = "0"+hh;
        if(min < 10)
            min = "0"+min;
        if(ss < 10)
            ss = "0"+ss;

        return dd+mm+yyyy+"_"+hh+min+ss;
    }
});