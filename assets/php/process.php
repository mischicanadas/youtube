<?php
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    
    if(isset($_POST['Action']))
    {
        $Result = "";
        
        $Action = $_POST["Action"];
        
        switch($Action)
        {
            case "Youtube": $Result = getMedia();break;
        }
        
        echo $Result;
    }

    function getMedia()
    {
        $URL = $_POST["URL"];
        $Format = $_POST["Format"];
        $Filename = $_POST["Filename"];
        //$WaterMark = $_POST["Watermark"];

        //Save in DB
        //$Query = "INSERT INTO historial (URL,Format,FileName) VALUES('$URL','$Format','$Filename')";
        //Ejecuta_Query($Query);

        $Json_Array = array();
        $Ext = "";
        $MediaFile = "";
        $Command = "";

        if($Format == "audio")
        {
            $Command = 'youtube-dl --extract-audio --audio-format mp3 -o "downloads/'.$Filename.'.%(ext)s" '.$URL;
            $Ext = "mp3";
        }
            
        if($Format == "video")
        {
            //-f 22 is Best Quality Format mp4
            $Command = 'youtube-dl -f 22 -o "downloads/'.$Filename.'.%(ext)s" '.$URL;
            $Ext = "mp4";
        }
        
        $MediaFile = "downloads/".$Filename.".".$Ext;

        run($Command);

        /*
        //Add Watermark text
        if($Format == "video")
        {
            //Get number of frames
            $Command_Frames = "ffprobe -select_streams v -show_streams downloads/".$Filename.".".$Ext." 2>/dev/null | grep nb_frames | sed -e 's/nb_frames=//'";
            $Frames = run($Command_Frames);
            //Set Text
            $Command_text = 'melt downloads/'.$Filename.'.'.$Ext.' -attach dynamictext:"'.$WaterMark.'" bgcolour=0x00000000 fgcolour=red valign=bottom in=0 out='.$Frames.' -consumer avformat:downloads/'.$Filename.'_Melt.'.$Ext;
            run($Command_text);
            $MediaFile = "downloads/".$Filename."_Melt.".$Ext;
        }
        */

        if($Command != "")
            $Json_Array['filename'] = $MediaFile;
        else
            $Json_Array['filename'] = "Unknow error";
        
        $Json = json_encode($Json_Array, JSON_NUMERIC_CHECK);
    
        return $Json;
    }

    function run($Command)
    {
        $res = exec($Command);
        return $res;
    }

    function Ejecuta_Query ($Query)
    {
        include("ConnDB.php");
        
        //Abre una conexion a MySQL server
        $mysqli = new mysqli($ConnDB["Servidor"],$ConnDB["Usuario"],$ConnDB["Password"],$ConnDB["DB"]);

        //Arrojo cualquier error tipo connection error
        if ($mysqli->connect_error) {
            die('Error : ('. $mysqli->connect_errno .') '. $mysqli->connect_error);
        }
        $result = $mysqli->query($Query);
        //Cierro la conexion 
        $mysqli->close();
        
        return $result;
    }
?>